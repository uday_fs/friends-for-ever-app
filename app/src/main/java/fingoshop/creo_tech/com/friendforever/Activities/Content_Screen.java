package fingoshop.creo_tech.com.friendforever.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import fingoshop.creo_tech.com.friendforever.R;

public class Content_Screen extends AppCompatActivity {
    private TextView bookText;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content__screen);

        bookText = (TextView) findViewById(R.id.bookText);
        imageView = (ImageView) findViewById(R.id.imageView);

        final Bundle bundle = getIntent().getExtras();
        if (bundle.getString("content") != null) {
            bookText.setText(Html.fromHtml(bundle.getString("content")));
        } else {
            bookText.setText(bundle.getString("Something is Wrong is Please try Again"));
        }


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String text = Html.fromHtml(bundle.getString("content")) + " \n  https://play.google.com/store/apps/details?id=fingoshop.creo_tech.com.friendforever";
                intent.setPackage("com.whatsapp");
                if (intent != null) {
                    intent.putExtra(Intent.EXTRA_TEXT, text);
                    startActivity(Intent.createChooser(intent, text));
                } else {
                    Toast.makeText(Content_Screen.this, "App not found", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
