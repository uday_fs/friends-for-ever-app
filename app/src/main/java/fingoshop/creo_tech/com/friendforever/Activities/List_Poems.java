package fingoshop.creo_tech.com.friendforever.Activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import fingoshop.creo_tech.com.friendforever.Classes.Data;
import fingoshop.creo_tech.com.friendforever.Classes.Deals_Adapter;
import fingoshop.creo_tech.com.friendforever.R;

public class List_Poems extends AppCompatActivity {
    public RecyclerView recyclerView;
    private List<Data> list;
    private RecyclerView.Adapter adapter;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list__poems);

        recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setNestedScrollingEnabled(false);

        progressDialog = ProgressDialog.show(List_Poems.this, null, null, false, true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progress_bar);
        progressDialog.setCancelable(false);


        Bundle bundle = getIntent().getExtras();
        if (bundle.getString("name") != null) {
            if (bundle.getString("name").equalsIgnoreCase("Love Shayri")) {
                Get_Data("https://checktricks.com/app/testsite/?json=get_tag_posts&tag_slug=love");
            } else if (bundle.getString("name").equalsIgnoreCase("Dosti Shayri")){
                Get_Data("https://checktricks.com/app/testsite/?json=get_tag_posts&tag_slug=dard");
            }
            else {
                Get_Data("https://checktricks.com/app/testsite/?json=get_tag_posts&tag_slug=friends");
            }
        }
    }

    private void Get_Data(String url) {
        progressDialog.show();
        list = new ArrayList<>();
        list.clear();
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                try {
                    JSONArray array = response.getJSONArray("posts");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = (JSONObject) array.get(i);
                        Data data = new Data();
                        data.setTitle(object.getString("title"));
                        data.setContent(object.getString("content"));
                        list.add(data);
                    }
                    adapter = new Deals_Adapter(list, List_Poems.this);
                    recyclerView.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

            }
        });

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(List_Poems.this);
        requestQueue.add(request);
    }
}
