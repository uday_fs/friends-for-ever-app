package fingoshop.creo_tech.com.friendforever.Classes;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fingoshop.creo_tech.com.friendforever.Activities.Content_Screen;
import fingoshop.creo_tech.com.friendforever.R;


public class Deals_Adapter extends RecyclerView.Adapter<Deals_Adapter.MyViewHolder> {

    private List<Data> Offerbanners_list = new ArrayList<>();
    private Context mContext;

    public Deals_Adapter(List<Data> Offerbanners_list, Context mContext) {
        this.Offerbanners_list = Offerbanners_list;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ayout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Data offers_Model = Offerbanners_list.get(position);

        holder.countryName.setText(offers_Model.getTitle());
        holder.offers_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,Content_Screen.class);
                intent.putExtra("content",offers_Model.getContent());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return Offerbanners_list.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView countryName;
        public LinearLayout offers_layout;

        public MyViewHolder(View view) {
            super(view);
            countryName = (TextView) view.findViewById(R.id.countryName);
            offers_layout = (LinearLayout)view.findViewById(R.id.layout);
        }
    }
}
